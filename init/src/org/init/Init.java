/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.init;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.page.MenuForm;

/**
 *
 * @author ilyab
 */
public class Init {    
    public static void main(String args[]) {
        try {
            MenuForm mf = new MenuForm();
            mf.Start();
        } catch (ParseException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}
