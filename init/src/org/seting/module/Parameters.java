/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seting.module;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ilya
 */

public class Parameters {
    private final String patch;
    private StringBuffer xml = new StringBuffer(); 
    private final ArrayList<String> NameParam;
    private final ArrayList<StringBuffer> ValueParam;

    public ArrayList<String> getNameParam() {
        return NameParam;
    }

    public ArrayList<StringBuffer> getValueParam() {
        return ValueParam;
    }
    
    public StringBuffer getValueParam(String name){
//        System.out.println("com.configurationv1.Parameters.getValueParam() name = "+name);
        int  num = NameParam.indexOf(name);
        if(num <0){
           addParam(name,"");
           return new StringBuffer("");
        }else
        return ValueParam.get(num);
    }
    
    public String getValueParamS(String name){
//        System.out.println("com.configurationv1.Parameters.getValueParam() name = "+name);
        int  num = NameParam.indexOf(name);
//        System.out.println("com.configurationv1.Parameters.getValueParam() num = "+num);
        return new String(ValueParam.get(num));
    }
    
    public void addParam(String NameParam, StringBuffer ValueParam){        
        this.NameParam.add(NameParam);
        this.ValueParam.add(ValueParam);
    }
        
    public void addParam(String NameParam, String ValueParam){        
        this.NameParam.add(NameParam);
        this.ValueParam.add(new StringBuffer(ValueParam));
    }
    
    public void editParam(String NameParam, long newValueParam){
        editParam(NameParam,String.valueOf(newValueParam));
    }
    
    public void editParam(String NameParam, String newValueParam){
        editParam(NameParam,new StringBuffer(newValueParam));
    }
    
    public void editParam(String NameParam, StringBuffer newValueParam){
        editParam(this.NameParam.indexOf(NameParam),newValueParam);
    }
    
    public void editParam(int idNameParam, StringBuffer newValueParam){
      ValueParam.set(idNameParam, newValueParam);
    }
    
    public ArrayList<String> getArrayValueParam(String name){        
        boolean Data = true;  
        StringBuffer param = ValueParam.get(NameParam.indexOf(name));
        ArrayList<String> ArrayValueParam = new ArrayList<>();
        while(Data){
        if(param.indexOf("<") != -1){
        String tag = param.substring(param.indexOf("<")+1,param.indexOf(">"));
        String valueTag;
        if(tag.length()> 0){
            int Start = param.indexOf("<"+tag+">");
            int End = param.indexOf("</"+tag+">");
            valueTag = param.substring(Start+2+tag.length(),End);
            param.delete(Start, End+3+tag.length());
        }else valueTag = "Not found value parametrs name:" +tag;
        ArrayValueParam.add(valueTag);
        }else Data = false;         
        }        
        return ArrayValueParam;
    }
    
    public void AddToArrayValueParam(String name,String ValueAdd){        
        boolean Data = true;  
        StringBuffer param =  getValueParam (name);// ValueParam.get(NameParam.indexOf(name));
        ArrayList<String> ArrayValueParam = new ArrayList<>();
        int Index = 0;
        int End = 0;
        while(Data){
        if(param.indexOf("<",End) != -1){
            int it = param.indexOf("<",End)+1;
            int ti = param.indexOf(">",it);
        String tag = param.substring(it,ti);
        if(tag.length()> 0){
            End = param.indexOf("</"+tag+">")+1;                        
        }
        Index = Integer.valueOf(tag)+1;
        }else Data = false;         
        }
        param.append(getParamXML(String.valueOf(Index),ValueAdd));
        editParam(name,param);
        
    }
    
    public Parameters(String patch){
        this.NameParam = new ArrayList<>();
        this.ValueParam = new ArrayList<>();
        this.patch = patch;
    }
    
    public void Initialize(){
        try {
//            System.out.println("Start LoadFileParam");
            LoadFileParam();
//            System.out.println("End LoadFileParam");
//            System.out.println("Start ProccesingData");
            ProccesingData();
//            System.out.println("End ProccesingData");
        } catch (FileNotFoundException ex) {
            SaveFileParam();
        } catch (IOException ex) {
            Logger.getLogger(Parameters.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    
    
    public void SaveParameters(){
        SaveFileParam();
    }
    
    private void ProccesingData(){
            Deserialization();                
    }
    
    private void Deserialization(){
        boolean Data = true;                
        while(Data){
        if(xml.indexOf("<") != -1){
        String tag = xml.substring(xml.indexOf("<")+1,xml.indexOf(">"));
        StringBuffer valueTag = new StringBuffer();
        if(tag.length()> 0){
            int Start = xml.indexOf("<"+tag+">");
            int End = xml.indexOf("</"+tag+">");
//            System.out.println("Start tag: <"+tag+"> index: "+Start);
//            System.out.println("Start tag: </"+tag+"> index: "+End);
            
            valueTag.append(xml.substring(Start+2+tag.length(),End));
            xml.delete(Start, End+3+tag.length());
        }else valueTag.append("Not found value parametrs name:").append(tag);
        NameParam.add(tag);
        ValueParam.add(valueTag);
        }else Data = false;         
        }
    }
    
     private void LoadFileParam() throws FileNotFoundException, IOException {
        String CodeStr;
        CodeStr = "UTF-8";
        String buffer = "";               
        BufferedReader reader = null;
        StringBuffer XMLOut =new StringBuffer();
        try{
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(patch), CodeStr));        
        while(buffer != null){
            buffer = reader.readLine();  
            if (buffer != null){        
                XMLOut.append(buffer);
            }
        }         
        int Start = XMLOut.indexOf("<Parametrs>")+11;
        int End = XMLOut.indexOf("</Parametrs>");        
        xml.append(XMLOut.substring(Start,End));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Parameters.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(reader != null)
                reader.close();
        }
    }
     
    private void SaveFileParam(){
        String CodeStr;
        CodeStr = "UTF-8";
         
        try(FileWriter writer = new FileWriter(patch, false))
        {
            writer.write("<Parametrs>\n");
            for(int index = 0; index < NameParam.size(); index++){                
                writer.write(getParamXML(index));
                writer.append('\n');
            }
            writer.write("</Parametrs>");             
            writer.flush();
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        } 
    }
    
    private String getParamXML(int id){
       return getParamXML(NameParam.get(id),ValueParam.get(id));
    }
    private String getParamXML(String id,String value){
        return getParamXML(id,new StringBuffer(value));
    }
    private String getParamXML(String id,StringBuffer value){
        return ("<"+id+">"+
                value+
                "</"+id+">");
    }
}
